FROM alpine

ENV KUBECTL_VERSION "1.20.7"
ENV HELM_VERSION "3.6.3"
ENV HELM_PLUGIN_DIFF_VERSION "3.1.1"
ENV HELM_PLUGIN_S3_VERSION "0.10.0"

RUN apk -U add bash jq py3-pip curl git \
    && pip3 install awscli

RUN wget -O /tmp/helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz \
  && mkdir /tmp/helm \
  && tar -zxvf /tmp/helm.tar.gz -C /tmp/helm \
  && mv /tmp/helm/linux-amd64/helm /usr/local/bin

ENV HELM_HOME "/root/.helm"
RUN mkdir -p "$HELM_HOME/plugins" \
    && helm plugin install https://github.com/databus23/helm-diff --version v${HELM_PLUGIN_DIFF_VERSION} \
    && helm plugin install https://github.com/hypnoglow/helm-s3.git --version v${HELM_PLUGIN_S3_VERSION}

# Install kubectl
RUN wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
  && chmod +x /usr/local/bin/kubectl


RUN curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.9/2020-08-04/bin/linux/amd64/aws-iam-authenticator \
  && chmod +x /usr/local/bin/aws-iam-authenticator

RUN mkdir -p /root/.kube
